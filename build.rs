#[cfg(windows)]
extern crate winres;

//use std::env;
//use std::path::Path;

fn main() {
    // windows only
    //println!("cargo:rustc-link-args=\"../protohand-lua/ico/app.res\"");
    #[cfg(windows)]
    if cfg!(target_os = "windows") {
        let mut res = winres::WindowsResource::new();
        res.set_icon("./app.ico"); // Replace this with the filename of your .ico file.
        res.compile().unwrap();
    }

    // set compile time constants
    println!("cargo:rustc-cfg=version=\"0.1.0\"");
    
}