use std::io::{Write};
use std::{net::TcpListener, thread::spawn, net::TcpStream};
use log::{debug, info, trace, error, warn};

use tungstenite::{
    protocol::WebSocket, accept_hdr,
    handshake::server::{ Response, Request, },
};

mod startup;

pub fn copy_args(args: &Vec<String>) -> Vec<String> {
    let mut ret = Vec::new();
    for a in args {
        ret.push(String::from(a));
    }
    ret
}

fn main() {
    // initialize logging
    if std::env::var("RUST_LOG").is_err() { std::env::set_var("RUST_LOG", "info"); }
    env_logger::init();

    let args = startup::arg_parse();
    let bnd = "127.0.0.1:".to_string() + &String::from(args.port.to_string());
    debug!("bind: {}", bnd);
    let index: &[u8] = include_bytes!("../index.html");

    let server = TcpListener::bind(bnd).unwrap();
    for raw_stream in server.incoming() {
        let peer_addr = args.copy_args(); // copy_args(&args.allow_from);
        spawn(move || {
            //let cloned_args = args.clone();
            debug!("stream: {:#?}", raw_stream);

            // handle connection errors
            let mut stream = match raw_stream {
                Err(e) => panic!("Failed to unwrap stream: {}", e),
                Ok(stream) => stream
            };
            debug!("stream: {:#?}", stream);
            info!("Connection from: {}", stream.peer_addr().unwrap());

            // check if peer is allowed to connect
            debug!("peer ip: {}", stream.peer_addr().unwrap().ip().to_string());
            let mut allowed_source_ip = false;
            for a in peer_addr {
                if a == stream.peer_addr().unwrap().ip().to_string() {
                    allowed_source_ip = true;
                    break;
                }
            }

            if ! allowed_source_ip {
                error!("Connections from {} are not allowed.", stream.peer_addr().unwrap().ip().to_string());
                return;
            }

            // preflight check. decide if we have to serve our static page or 
            // if this is a proper socket request
            let mut buf = [0; 4096];
            let len = stream.peek(&mut buf).unwrap();
            let req_header: String = String::from_utf8(buf[0..len].to_vec()).unwrap();
            let req_header2: String;
            debug!("len: {:#?}", len);
            debug!("req_header: {:#?}", req_header);

            let mut upgrade: bool = false;
            let mut hbuffer = [httparse::EMPTY_HEADER; 124];
            let mut hdr = httparse::Request::new(&mut hbuffer);
            let res = hdr.parse(&req_header.as_bytes()).unwrap();
            if res.is_partial() {
                trace!("needing larger buffer for request header");
                let mut buf = [0; 8192];
                let len = stream.peek(&mut buf).unwrap();
                req_header2 = String::from_utf8(buf[0..len].to_vec()).unwrap();

                hdr = httparse::Request::new(&mut hbuffer);
                let res = hdr.parse(&req_header2.as_bytes()).unwrap();
                if res.is_partial() {
                    error!("Too large request header, ignoring request");
                    return;
                }
            }
            debug!("uri: {}", hdr.path.unwrap());
            trace!("Request Header:\n{:#?}", hdr);

            // only GET is supported
            if hdr.method.unwrap() != "GET" {
                error!("HTTP method {} is not allowed.", hdr.method.unwrap());
                return;
            }

            // check if we need to upgrade
            for h in hdr.headers {
                if h.name.to_lowercase() == "upgrade" && h.value == "websocket".as_bytes() {
                    upgrade = true;
                }
            }

            // this is a http request, without protocol upgrade, serve accordingly
            if !upgrade {

                // we serve '/' and '/index.html'
                if hdr.path.unwrap() == "/" || hdr.path.unwrap() == "/index.html" {
                    let response = format!("HTTP/1.1 200 OK\r\n\
                        Content-Length: {}\r\n\
                        Content-Type: text/html\r\n\
                        \r\n{}", index.len(), String::from_utf8(index.to_vec()).unwrap());

                    match stream.write(response.as_bytes()) {
                        Err(e) => error!("Failed to send response: {}", e),
                        Ok(_)  => info!("GET {} 200", hdr.path.unwrap()),
                    };
                    return;
                }

                // 404, not found
                match stream.write("HTTP/1.1 404 Not Found\r\n\r\n".as_bytes()) {
                    Err(e) => error!("Failed to send response: {}", e),
                    Ok(_)  => warn!("GET {} 404", hdr.path.unwrap())
                };
                return;
            }

            // we have a socket request, open connection
            let callback = |req: &Request, mut response: Response| {
                trace!("stream: {:#?}", req);
                // debug!("The request's path is: {}", req.uri().path());

                // Let's add an additional header to our response to the client.
                let headers = response.headers_mut();
                //headers.append("MyCustomHeader", ":)".parse().unwrap());
                //headers.append("SOME_TUNGSTENITE_HEADER", "header_value".parse().unwrap());
                debug!("Handshake response headers\n{:#?}", headers);

                Ok(response)
            };
            let mut websocket: WebSocket<TcpStream> = accept_hdr(stream, callback).unwrap();

            // servce the socket
            loop {
                if ! websocket.can_read() {
                    info!("Connection closed by peer");
                    break;
                }

                let msg = websocket.read_message().unwrap();
                if msg.is_binary() || msg.is_text() {
                    info!("got message: {:#?}", msg.to_text().unwrap());
                    websocket.write_message(msg).unwrap();
                }
            }

            info!("Shutting down socket");
        });
    }
}

