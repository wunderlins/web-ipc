#![allow(dead_code)]
use log::{debug};
use clap::Parser;

/// Simple program to greet a person
#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct _Args {
    /// allow connections from these ips, default: 127.0.0.1, comma separated list
    #[clap(short, long)]
    allow_from: Option<String>,

    /// Port number to bind
    #[clap(short, long, default_value_t = 3012)]
    port: u16,
}

#[derive(Debug, Clone)]
pub struct Args {
    pub allow_from: Vec<String>,
    pub port: u16,
}

impl Args {
    pub fn copy_args(&self) -> Vec<String> {
        let mut ret = Vec::new();
        for a in &self.allow_from {
            ret.push(String::from(a));
        }
        ret
    }
}

pub fn arg_parse() -> Args {
    let args = _Args::parse();

    // handle defaults
    let allow_from: String = match args.allow_from.clone() {
        None    => String::from("127.0.0.1"),
        Some(v) => v.clone()
    };

    let allow_from: Vec<_> = allow_from.split(",").collect();
    let mut cpy: Vec<String> = Vec::new();
    for e in allow_from {
        debug!("{}", e);
        cpy.push(e.to_string());
    }

    let ret = Args {
        allow_from: cpy,
        port: args.port
    };

    debug!("args:\n{:#?}", ret);
    ret
}